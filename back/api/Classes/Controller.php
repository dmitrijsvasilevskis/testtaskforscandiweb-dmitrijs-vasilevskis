<?php

class Controller
{

    // returns json with all database posts
    public static function show()
    {
        return (new Products)->getAllProducts();
    }

    // create new post in database
    public static function create()
    {
        $inputsData = json_decode(file_get_contents("php://input"),true);
        // receive data from post request
        $create = new Validator($inputsData);
        
    }
    // delete posts from database 
    public static function delete()
    {
        $inputsData = json_decode(file_get_contents("php://input"),true);
        // receive data from post request
        $delete = (new Products)->deleteProducts($inputsData);

    }

}

?>