<?php 

class Database
{

    private $conn; 
    private $host = "localhost";
    private $database_name = "product_list";
    private $username = "root";
    private $password = "";   

    function __construct() {
        try{
        $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            echo 'Conn error' . $e->getMessage();
        }

    }

    function getConnection(){
        return $this->conn;
    }
}

?>