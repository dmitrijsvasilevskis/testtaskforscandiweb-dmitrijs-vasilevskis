<?php 

class DVD extends Products
 {
    protected $inputData;

    function __construct(array $data)
    {
        parent::__construct();

        $this->inputData = $data;

        // saving incoming data in class variables
        //  so we can work with different values easily

        $this->sku = $data['sku'];
        $this->name = $data['name'];
        $this->price = $data['price'];
        $this->type = $data['type'];
        $this->attribute = $data['attribute'];

    }
    
    // we alredy get we alredy get validated data
    //  by react function, just showing that we can validate data in classes
    public function attributeValidation(array $data)
    {
        if(is_numeric($data['attribute']) && floatval($data['attribute']>=0))
        {
            $this->attribute = $data['attribute'].'MB';
            $this->inputData['attribute'] = $this->attribute;
        }
    }

}

?>