<?php 

class Furnuture extends Products
 {
    protected $inputData;

    function __construct(array $data)
    {
        parent::__construct();
        $this->inputData = $data;

        // saving incoming data in class variables
        //  so we can work with different values easily
        $this->sku = $data['sku'];
        $this->name = $data['name'];
        $this->price = $data['price'];
        $this->type = $data['type'];
        $this->attribute = $data['attribute'];
        
    }

    // we alredy get data in right dimensons(20x20x20) validated
    //  by react function, just showing that we can validate data in classes 
    public function attributeValidation(array $data)
    {
        if(is_string($data['attribute']))
        {
            $this->attribute = $data['attribute'].'CM';
            $this->inputData['attribute'] = $this->attribute;
        }
    }

}

?>