<?php

class Products extends QueryBuilder
{

    
    private $conn;
    private $table_name = 'products';

    protected $inputData;
    protected $sku; 
    protected $name;
    protected $price;
    protected $type;
    protected $attribute;
    protected $data = array();


    public function __construct(){
        parent::__construct($this->table_name);
    }


    public function getAllProducts()
    {
        $res = $this->selectAll();
        echo json_encode($res);
    }


    public function createPost()
    {
        return $this->insert($this->inputData);
    }

    public function deleteProducts(array $data)
    {
        // sending array of data, param and action
        return $this->delete($data,'id','IN');
    }
    
}
?>