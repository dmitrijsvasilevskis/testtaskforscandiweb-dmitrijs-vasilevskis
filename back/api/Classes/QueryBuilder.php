<?php

abstract class QueryBuilder
{

    private $db;
    private $query ='';
    private $table_name;
    private $stmt;
    private $data = array();


    function __construct($table_name)
    {
        $this->db = (new Database)->getConnection();
        $this->table_name = $table_name;
    }

    // set query
    public function query($query)
    {
       return $this->query = $query;
        
    }

    public function selectAll()
    {
        $this->query('SELECT * FROM ' . $this->table_name . '');
        $this->prepare()->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete(array $data, string $param, string $action)
    {
        $this->data = $data;
        $this->query('DELETE FROM ' . $this->table_name . '');
        $this->where($param, $action);
        $this->prepare();
        return $this->execute();
        
    }


    public function insert(array $data)
    {
        $this->data = $data;

        // geting array of specific name values
            $value=array();
            foreach($data as $key=> $val){
                array_push($value, ':'.$key);
            }
        $this->query('INSERT INTO '. $this->table_name . '('. implode(',',(array_keys($data))).') VALUES('. implode(',',$value).')');
        return $this->bindParams();
    }

    // run prepared statement pdo function
    public function prepare()
    {
        return $this->stmt = $this->db->prepare($this->query);
    }

    // execute preprared statement
    public function execute()
    {
        return $this->stmt->execute();
    }

    // update query
    public function where(string $param,string $action)
    {
        $this->query .= ' WHERE '. $param .' '. $action .' ('. implode(',',$this->data) .')';
        return $this;
    }

    // binding params in query name variable
    public function bindParams()
    {
        $this->prepare();
        foreach(array_keys($this->data) as $field){
            $this->stmt->bindParam(':'.$field, $this->data[$field]);
        }
        $this->execute();
    }

    // get params of variable
    public function getParam($value)
    {
        $type = match(gettype($value)){
            'string' => PDO::PARAM_STR,
            'integer' => PDO::PARAM_INT,
        };
    }

}

?>