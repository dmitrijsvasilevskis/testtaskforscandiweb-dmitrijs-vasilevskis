<?php

class Router
{

    private $path = array();

    public function set($path, $controller, $method){
        array_push($this->path,array($path, $controller, $method));
    }

    public function run(){
        $key = array_search($_SERVER['PATH_INFO'], array_column($this->path,0));
        if($this->path[$key][2]=='get'){
            return call_user_func($this->path[$key][1]);
        }else{
            return call_user_func($this->path[$key][1], $_POST);
        }
    }
    
}

?>