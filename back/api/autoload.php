<?php

function classes($classname)
{
    $paths = array(
        'Classes/',
        'Classes/Product/'
    );

    foreach($paths as $path){
        if(file_exists($path.$classname.'.php')){
            include_once($path.$classname.'.php');
        }
    }
}

spl_autoload_register('classes');


?>