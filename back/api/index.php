<?php
include 'autoload.php';
include 'config.php';


$router = new Router();

// creating custom routes
$router->set('/products/get', 'Controller::show', 'get');
$router->set('/products/add', 'Controller::create', 'post');
$router->set('/products/delete', 'Controller::delete', 'post');

// run controller function that given in set function by http link request
$router->run();


?>