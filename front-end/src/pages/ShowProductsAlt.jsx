import React from 'react';
import Button from '../components/UI/button/Button';
import {Link} from 'react-router-dom'
import ProductList from '../components/ProductList';


class ShowProductsAlt extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            posts: [],
            checkedBoxes: [],
            isLoaded: false
        };
    }
    componentDidMount() {
        fetch('https://mercscanditesttask.000webhostapp.com/api/products/get')
            .then(response => response.json())
            .then(
                (result) => {
                    this.setState({
                        posts: result,
                        isLoaded: true
                    });
                },
                (error) => {
                    this.setState({
                        error,
                        isLoaded: false
                    });
                }
            )
    }



    handleDelete = () => {
        console.log(this.state.checkedBoxes)
        fetch('https://mercscanditesttask.000webhostapp.com/api/products/delete', {
            method: 'POST',
            body: JSON.stringify(this.state.checkedBoxes),
            headers: { 'Content-Type': 'application/json'}
        })
            .then(response => {
            if (response.status == 200) {
                console.log('delete succesfuly')
            }
            })
            .then(window.location.href = '/')
            .catch((error) => {
                throw(error);
            
        })
        
        
    }

    onChange = (e) => {
        let checkedBoxes = this.state.checkedBoxes
        let posts = this.state.posts
        posts.forEach(post => {
            if (post.id === e.target.id && e.target.checked) {
                checkedBoxes.push(post.id)
                    }else if(!e.target.checked && post.id === e.target.id ) {
                checkedBoxes.splice(checkedBoxes.indexOf(e.target.id), 1)
            }
        })
    }

    render() {
        const { isLoaded, error, posts } = this.state;
        return(
            <div>
                { console.log(this.state.posts)}
                <div className='navbar'>
                    <h1>Product List</h1>
                    <div>
                        <Button><Link to='/add-product'>ADD</Link></Button>
                        <Button id='delete-product-btn' onClick={()=>{this.handleDelete()}} >MASS DELETE</Button>
                    </div>
                </div>
                <hr />
                {posts && <ProductList onChange={this.onChange} posts={this.state.posts}/>}
            </div>
            
        );
    }
 
}

export default ShowProductsAlt;


